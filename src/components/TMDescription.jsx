import React from "react";
import styles from "./TMDesciption.module.css";
import TM from "../resources/TM.rar";
import TMDB from "../resources/TMDB.bacpac";

export default function TMDesciption() {
  return (
    <>
      <div className={styles.desciption}>
        <div className={styles.TitleDesc}>
          Description: (The final version is coming)
        </div>
        <p1>Title: Trainee management</p1>
        <p1>A desktop application, </p1>
        <p1>to manage college interns</p1>
        <p1>Technology: wpf</p1>
        <p1>Language: C#</p1>
        <p1>DB: Sql</p1>
        <p1 className={styles.links}>
          <a className={styles.Down} href={TM} download>
            Trainee management.rar (78 Ko)
          </a>
        </p1>
        <p1 className={styles.links}>
          <button className={styles.Down} href={TMDB} download>
            TraineeManagementDB.bac (5 Ko)
          </button>
        </p1>
      </div>
    </>
  );
}
