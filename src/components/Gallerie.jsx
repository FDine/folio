import ImgHome from "../resources/ImgHome.PNG";
import ImgProgram from "../resources/ImgProgram.PNG";
import ImgStagiaires from "../resources/ImgStagiaires.PNG";
import styles from "./Gallerie.module.css";
import React, { useState, useEffect } from "react";
import { FiChevronRight, FiChevronLeft } from "react-icons/fi";

export default function GallerieData(props) {
  let imgs = [{ src: ImgHome }, { src: ImgProgram }, { src: ImgStagiaires }];

  const [index, setIndex] = useState(0);

  useEffect(() => {
    setIndex(0);
  }, []);

  const next = () => {
    if (index === imgs.length - 1) {
      setIndex(0);
    } else {
      setIndex(index + 1);
    }
  };
  const prev = () => {
    if (index === 0) {
      setIndex(imgs.length - 1);
    } else {
      setIndex(index - 1);
    }
  };
  return (
    <div className={styles.Gallerie}>
      <button onClick={prev}>
        <FiChevronLeft />{" "}
      </button>
      <img src={imgs[index].src} alt='' />
      <button onClick={next}>
        <FiChevronRight />
      </button>
    </div>
  );
}
