import React, { useState } from "react";
import styles from "./SeeWorks.module.css";
import { BsJustify } from "react-icons/bs";

export default function MenuToggle(props) {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <div className={styles.MenuToggle}>
      <div className={styles.titleMenu} onClick={togglecontent}>
        <BsJustify className={styles.Icon} />
      </div>
      {visible && (
        <div className={styles.liens}>
          <h5 onClick={props.changePage("accueil")}>
            {" "}
            <h3 onClick={togglecontent}>Home</h3>{" "}
          </h5>
          <h5 onClick={props.changePage("AppBureau")}>
            <h3 onClick={togglecontent}>Desktop Application</h3>
          </h5>
          <h5 onClick={props.changePage("website")}>
            <h3 onClick={togglecontent}>WebSite</h3>
          </h5>
        </div>
      )}
    </div>
  );
}
