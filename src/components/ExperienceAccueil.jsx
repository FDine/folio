import React, { useState } from "react";
import styles from "./ExperienceAccueil.module.css";

export default function ExperienceAccueil() {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.title} onClick={togglecontent}>
          Diploms & Experience
        </div>

        {visible && (
          <div>
            <p1>Two diploma in international baccalaureate</p1>
            <p1>DEC in Computer Programming</p1>
            <p1>Two years experience of computer repair </p1>
            <p1>One year experience of flashing phones </p1>
          </div>
        )}
      </div>
    </>
  );
}
