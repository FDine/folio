import React from "react";
import styles from "./PagesDesc.module.css";
import PageMenuDesc from "./PageMenuDesc.jsx";
import PagePanierDesc from "./PagePanierDesc.jsx";
import PageCommandeDesc from "./PageCommandeDesc.jsx";
import PageLogInDesc from "./PageLogInDesc.jsx";
import PageSignIn from "./PageSignIn.jsx";

export default function PagesDesc() {
  return (
    <div className={styles.PageDesc}>
      <PageMenuDesc />
      <PagePanierDesc />
      <PageCommandeDesc />
      <PageLogInDesc />
      <PageSignIn />
    </div>
  );
}
