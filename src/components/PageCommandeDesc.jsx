import styles from "./PagesDesc.module.css";
import React, { useState } from "react";
export default function Section_PageCommandeDesc() {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Order page
        </div>

        {visible && (
          <div>
            (Commande)
            <p1>-Only cafeteria employees have access to this page</p1>
            <p1>-It displays all commands and their status</p1>
            <p1>-Employees can change order status in this page</p1>
          </div>
        )}
      </div>
    </>
  );
}
