import styles from "./PagesDesc.module.css";
import React, { useState } from "react";
export default function Section_PagePanierDesc() {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Log In page
        </div>

        {visible && (
          <div>
            (Connexion)
            <p1>
              -The user can connect to the site with his email address and his
              password
            </p1>
          </div>
        )}
      </div>
    </>
  );
}
