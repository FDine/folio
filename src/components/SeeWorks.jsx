import React, { useState } from "react";
import styles from "./SeeWorks.module.css";

export default function SeeWorks(props) {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div className={styles.title} onClick={togglecontent}>
        See Works
      </div>
      {visible && (
        <div className={styles.liens}>
          <h5 onClick={props.changePage("AppBureau")}>Desktop Application</h5>
          <h5 onClick={props.changePage("website")}>WebSite</h5>
        </div>
      )}
    </>
  );
}
