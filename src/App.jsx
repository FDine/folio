import { useState } from "react";

import Header from "./components/Header";
import Accueil from "./pages/Accueil";
import AppBureau from "./pages/AppBureau";
import Website from "./pages/Website";

export default function App() {
  const [pageCourante, setPageCourante] = useState("accueil");

  const changePage = (page) => {
    return () => {
      setPageCourante(page);
    };
  };

  return (
    <>
      <Header changePage={changePage} />
      {pageCourante === "accueil" && <Accueil changePage={changePage} />}
      {pageCourante === "AppBureau" && <AppBureau />}
      {pageCourante === "website" && <Website />}
    </>
  );
}
