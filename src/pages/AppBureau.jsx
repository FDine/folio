import React from "react";
import styles from "./AppBureau.module.css";
import TMDesciption from "../components/TMDescription";
import TMPages from "../components/TMPages";
import Gallerie from "../components/Gallerie.jsx";

export default function AppBureau() {
  return (
    <div className={styles.AppB}>
    <div className={styles.Appdescription}>  <TMDesciption  /> </div>
      <div className={styles.Image}>
        <Gallerie />
      </div >
     <div className={styles.Pagedescription}><TMPages  /></div> 
    </div>
  );
}
