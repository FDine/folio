import React from "react";
import styles from "./Website.module.css";
import DescriptionWebsite from "../components/DescriptionWebsite.jsx";
import GallerieWebsite from "../components/GallerieWebsite.jsx";
import PagesDesc from "../components/PagesDesc";
export default function Website() {
  return (
    <div className={styles.Website}>
      <div className={styles.DescWeb}>
        <DescriptionWebsite />
      </div>

      <div className={styles.ImageWeb}>
        <GallerieWebsite />
      </div>
      <div>
        <PagesDesc />
      </div>
    </div>
  );
}
